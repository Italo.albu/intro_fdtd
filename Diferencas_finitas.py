import numpy as np
import matplotlib.pyplot as plt
import time

N = 30 ##N**2 dá o número de pontos na malha
p_esq = 100
p_cima = 70
p_dir = 35
p_baixo = 0

def calc_M(N):
    if (N < 1):
        return
    tam = N*N
    M = 4*np.eye(tam) #matriz N**4 
    for i in range(0,tam):
        if(((i+1)%N)!=0):
            M[i][i+1] = -1
        if((i%N)!=0):
            M[i][i-1] = -1
        if(i<(N-1)*N):
            M[i][i+N] = -1
        if(i>N-1):
            M[i][i-N] = -1
    return M

def calc_R(A,B,C,D,N):
    if(N<=0):
        return
    elif(N == 1):
        R = [A+B+C+D]
    else:
        R = np.array([A+B] + [B]*(N-2) + [B+C] + ([A]+[0]*(N-2)+[C])*(N-2) + [A+D]+[D]*(N-2) + [C+D])
    return np.transpose(R)

start = time.time()
R = calc_R(p_esq,p_cima,p_dir,p_baixo,N)
end = time.time()
print(end - start)

start = time.time()
M = calc_M(N)
end = time.time()
print(end - start)

start = time.time()
V = np.dot(R,np.linalg.inv(M))
end = time.time()
print(end - start)

start = time.time()
malha = []
for i in range(0,N*N,N):
    malha.append(list(V[i:i+N]))
malha  =  np.array(malha)
end = time.time()
print(end - start)

start = time.time()
plt.imshow(malha)
plt.colorbar()
plt.show()
#plt.savefig('foo.png')
end = time.time()
print(end - start)



# A[i][j] = A[j][i] = A[N-i-1][N-j-1] = A[N-j-1][N-i-1] = -1



