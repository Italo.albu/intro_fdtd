import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import spsolve

xi = 0; yi = 1; #inicial
xf = 11; yf = 0; #final

h = 1e-2;
N = int((xf-xi)/h) + 1;

M = sparse.diags([2-h, 2*h*h-4, 2+h], [-1, 0, 1], shape=(N, N)).toarray();
M[0][0] = 1;
M[0][1] = 0;
M[-1][-2] = 0;
M[-1][-1] = 1;

B = np.zeros(N);
B[0] = yi;
B[-1] = yf;

M = sparse.csr_matrix(M)
y = spsolve(M,B)
x = np.arange(xi,xf+h,h)

plt.figure("Comparação das funções obtidas")
plt.subplot(1,2,1)
plt.title("Aproximação por diferenças finitas (h=%f)"%h)
plt.plot(x,y)
plt.xlabel('x')
plt.ylabel('y')
plt.grid()

f = np.exp(-0.5*x)*(np.cos(0.5*np.sqrt(3)*x) - np.sin(0.5*np.sqrt(3)*x)/np.tan(0.5*np.sqrt(3)*11))
plt.subplot(1,2,2)
plt.title("Obtida analiticamente")
plt.plot(x,f)
plt.xlabel('x')
plt.ylabel('y')
plt.grid()

erro = [0]*len(f)
for i in range(0,len(f)):
    if (f[i] != 0):
        erro[i] = (f[i]-y[i])*100/f[i]
        
plt.figure("Erro na aproximação")
plt.title("Erro na aproximação (h=%f)"%h)
plt.plot(x,erro)
plt.xlabel('x')
plt.ylabel('erro(%)')
plt.grid()

plt.tight_layout()
plt.show()




'''
M = sparse.csr_matrix((N,N), dtype = np.int8)

print(M.toarray())
'''

'''
 1 0 0 0 0 0 0 0 0 0 0 0 y0    1
 1-2 3 0 0 0 0 0 0 0 0 0 y1    0
 0 1-2 3 0 0 0 0 0 0 0 0 y2    0
 0 0 1-2 3 0 0 0 0 0 0 0 y3    0
 0 0 0 1-2 3 0 0 0 0 0 0 y4    0
 0 0 0 0 1-2 3 0 0 0 0 0 y5    0
 0 0 0 0 0 1-2 3 0 0 0 0 y6    0
 0 0 0 0 0 0 1-2 3 0 0 0 y7    0
 0 0 0 0 0 0 0 1-2 3 0 0 y8    0
 0 0 0 0 0 0 0 0 1-2 3 0 y9    0
 0 0 0 0 0 0 0 0 0 1-2 3 y10   0
 0 0 0 0 0 0 0 0 0 0 0 1 y11   0
'''  





  
 
