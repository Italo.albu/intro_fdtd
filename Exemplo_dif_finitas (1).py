import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse

xi = 0
xf = 11
h = 5e-3
N = int((xf-xi)/h)

M = (2*h*h-4)*np.eye(N);
M[0][1] = 2+h
for i in range(1,N-1):
    M[i][i-1] = 2-h;
    M[i][i+1] = 2+h
M[N-1][N-2] = 2-h

B = np.zeros(N)
B[0] = -1

y = np.linalg.solve(M,B)

plt.subplot(1,2,1)
plt.plot(np.arange(0,N),y,label = "Aproximação por diferenças finitas")
plt.xlabel('x')
plt.ylabel('y(x)')
plt.grid()
plt.legend()


x = np.linspace(0,xf,N)
f = np.exp(-0.5*x)*(np.cos(0.5*np.sqrt(3)*x) - np.sin(0.5*np.sqrt(3)*x)/np.tan(0.5*np.sqrt(3)*11))
plt.subplot(1,2,2)
plt.plot(x,f,label = "Obtida analiticamente")
plt.xlabel('x')
plt.ylabel('y(x)')
plt.grid()
plt.legend()


plt.tight_layout()
plt.show()
'''
M = sparse.csr_matrix((N,N), dtype = np.int8)
M[0][0] = -2
M[0][1] = 3
for i in range(1,N-1):
    M[i][i-1] = 1;
    M[i][i+1] = 3
M[N-1][N-1] = -2
M[N-1][N-2] = 1
print(M.toarray())
'''
'''
-2 3 0 0 0 0 0 0 0 0 y1   -1
 1-2 3 0 0 0 0 0 0 0 y2    0
 0 1-2 3 0 0 0 0 0 0 y3    0
 0 0 1-2 3 0 0 0 0 0 y4    0
 0 0 0 1-2 3 0 0 0 0 y5    0
 0 0 0 0 1-2 3 0 0 0 y6    0
 0 0 0 0 0 1-2 3 0 0 y7    0
 0 0 0 0 0 0 1-2 3 0 y8    0
 0 0 0 0 0 0 0 1-2 3 y9    0
 0 0 0 0 0 0 0 0 1-2 y10   0
'''  





  
 
