import numpy as np

N = 3 # N**2 dá o número de pontos na malha
p_esq = 10
p_cima = 0
p_dir = 0
p_baixo = 0

def calc_M(N):
    M = np.array([[0]*N*N]*N*N) #matriz N**4 
    for i in range(0,N*N):
        M[i][i] = 4
        if(((i+1)%N)!=0):
            M[i][i+1] = -1
        if((i%N)!=0):
            M[i][i-1] = -1
        if(i<(N-1)*N):
            M[i][i+N] = -1
        if(i>N-1):
            M[i][i-N] = -1
    return M

def calc_R(A,B,C,D,N):
    R = np.array([A+B] + [B]*(N-2) + [B+C] + ([A]+[0]*(N-2)+[C])*(N-2) + [A+D]+[D]*(N-2) + [C+D])
    return np.transpose(R)

R = calc_R(p_esq,p_cima,p_dir,p_baixo,N)
M = calc_M(N)
V = np.dot(R,np.linalg.inv(M))

print(M)
print(V)