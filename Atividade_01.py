import matplotlib.pyplot as plt
import numpy as np

###inicializando variaveis
list_dx = [1e-3, 1e-2, 1e-1]

xi = 0
xf = 5

list_N = []        #número de elementos de cada lista x a depender do delta x
list_x = []        #eixos x para cada delta x
list_fx = []       #função de x para cada delta x
list_deriv_fx = [] #derivada exata de x para cada delta x
N_dx = len(list_dx)

for i in range(0, N_dx):
    list_N.append(int((xf - xi)/list_dx[i]))
    list_x.append(np.linspace(xi,xf,list_N[i]))
    list_fx.append(np.exp(list_x[i]))
    list_deriv_fx.append(np.exp(list_x[i]))
    
#variáveis referentes a aproximação e erro
#aproximações de derivada de fx
fl_p = []
fl_r = []
fl_c = []
#erro em cada tipo de aproximação
erro_p = []
erro_r = []
erro_c = []
for i in range(0, N_dx):
    fl_p.append([0]*list_N[i])
    fl_r.append([0]*list_N[i])
    fl_c.append([0]*list_N[i])
    erro_p.append([0]*list_N[i])
    erro_r.append([0]*list_N[i])
    erro_c.append([0]*list_N[i])
    
### calculando derivadas e erro
for j in range(0, N_dx):
    for i in range(0, list_N[j]-1):
        fl_p[j][i]= (list_fx[j][i+1] - list_fx[j][i])/list_dx[j]
        erro_p[j][i] = np.absolute((fl_p[j][i] - list_deriv_fx[j][i])*100/list_deriv_fx[j][i])
    for i in range(1,list_N[j]):
        fl_r[j][i] = (list_fx[j][i] - list_fx[j][i-1])/list_dx[j]
        erro_r[j][i] = np.absolute((fl_r[j][i] - list_deriv_fx[j][i])*100/list_deriv_fx[j][i])
    for i in range(1,list_N[j]-1):
        fl_c[j][i]= (list_fx[j][i+1] - list_fx[j][i-1])/(2*list_dx[j])
        erro_c[j][i] = np.absolute((fl_c[j][i] - list_deriv_fx[j][i])*100/list_deriv_fx[j][i])
        
### plotando resultados

def print_graf22(id_plot, x, y, label_x, label_y, title):
    plt.subplot(2,2,id_plot);
    plt.plot(x,y)
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.title(title)
    
def print_graf(x, y, label_x, label_y, legend):
    plt.plot(x,y,label = legend)
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.legend()
    
#gráficos das aproximações
for i in range(0,N_dx):
    plt.figure(i);
    plt.title("Comparação das derivadas para dx = %f" % list_dx[i])
    print_graf(list_x[i][:list_N[i]-2],fl_p[i][:list_N[i]-2],"x","f'(x)", "Progressiva")
    print_graf(list_x[i][1:],fl_r[i][1:],"x","f'(x)", "Regressiva")
    print_graf(list_x[i][1:list_N[i]-2],fl_c[i][1:list_N[i]-2],"x","f'(x)", "Central")
    print_graf(list_x[i], list_deriv_fx[i],"x","f'(x)", "Derivada exata de f(x)")

#gráficos de erro

for i in range(0,N_dx):
    plt.figure(i+N_dx);
    plt.title("Comparação dos erros para dx = %f" % list_dx[i])
    print_graf(list_x[i][:list_N[i]-2],erro_p[i][:list_N[i]-2],"x","Erro(%)", "Erro: Progressiva")
    print_graf(list_x[i][1:],erro_r[i][1:],"x","Erro(%)", "Erro: Regressiva")
    print_graf(list_x[i][1:list_N[i]-2],erro_c[i][1:list_N[i]-2],"x","Erro(%)", "Erro: Central")

plt.show()

#erro médio em cada derivada



#plt.scatter(x,y) mostra pontos



