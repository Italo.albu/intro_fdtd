import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse

dx = 1e-2
xi = 0
xf = 11
N = int((xf-xi)/dx)

M = (2*dx*dx-4)*np.eye(N)
M[0][1] = 2+dx
for i in range(1,N-1):
    M[i][i-1] = 2-dx
    M[i][i+1] = 2+dx
M[N-1][N-2] = 2-dx

B = np.zeros(N)
B[0] = -1

fx = np.linalg.solve(M,B)

plt.subplot(1,2,1)
plt.plot(np.arange(0,N),fx,label = "Cálculo por Diferenças Finitas")
plt.xlabel('x')
plt.ylabel('f(x) - y')
plt.grid()
plt.legend()

x = np.linspace(xi,xf,N)
f = np.exp(-0.5*x)*(np.cos(0.5*np.sqrt(3)*x) - np.sin(0.5*np.sqrt(3)*x)/np.tan(0.5*np.sqrt(3)*11))
plt.subplot(1,2,2)
plt.plot(x,f,label = "Cálculo Análitico")
plt.xlabel('X')
plt.ylabel('f(x) - y')
plt.grid()
plt.legend()

plt.tight_layout()
plt.show()

M = sparse.csr_matrix((N,N), dtype = np.int8)
M.toarray()
M[0][1:N] = 0
M[0][0] = 1
M[N-1][0:N-1] = 0
M[N-1][N-1] = 1

print(M)

# 
# for i in range(1,N-1):
#     M[i][i-1] = 1;
#     M[i][i+1] = 3
# M[N-1][N-1] = -2
# M[N-1][N-2] = 1
# print(M.toarray())
