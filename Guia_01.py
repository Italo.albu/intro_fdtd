import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as lgs
import scipy.linalg as lg
#from scipy.linalg import eigh_tridiagonal, eigvalsh_tridiagonal

lam0  = 1
n1 = 2
n2 = 1
a = 2*lam0
b = 5*lam0
k0 = 2*np.pi/lam0

Nres = 2
dx = lam0/Nres
Sx = a+2*b

Nx = int(np.ceil(Sx/dx))
Sx = Nx*dx

nx_a = round(a/dx)
nx_1 = round((Nx-nx_a)/2)
nx_2 = Nx-nx_1

N = np.zeros((Nx,1))
N[:nx_1] = n2
N[nx_1:nx_2] = n1
N[nx_2:Nx] = n2

N = sp.spdiags(N.T,0,Nx,Nx)
Dxx = k0**2/dx*sp.diags([1,-2,1],[-1,0,1],shape = (Nx,Nx))

A = (Dxx + N**2)

#Comandos da Scipy.sparse.linalg
w, v = lgs.eigs(A)
W, V = lgs.eigsh(A)

#Comandos da Scipy.linalg
#va, ve = lg.eig(A)
#Va, Ve = lg.eigh(A)
#va, ve = lg.eigvals(A)
#Va, Ve = lg.eigvalsh(A)
#va, ve = lg.eig_banded(A)
#Va, Ve = lg.eigvals_banded(A)
###va, ve = eigh_tridiagonal(A)
###Va, Ve = eigvalsh_tridiagonal(A)

#Comandos da Numpy.linalg
#c, d = la.eigvals(A)
#e, f = la.eigvalsh(A)
#c, d = la.eig(A)
#e, f = la.eigh(A)

