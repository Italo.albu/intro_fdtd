import matplotlib.pyplot as plt
import numpy as np
import sympy as sp

def calc_X(x_): 
    X_t = []
    x_ = np.array(x_)
    for i in range(0,len(x_)):
        X_t.append(x_**i)
    X = np.transpose(X_t)
    return X

def print_graf12(id_plot, x, y, label_x, label_y,legend):
    plt.subplot(1,2,id_plot);
    plt.plot(x,y,label = legend)
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.legend()

## Código com os valores

h = 1
x_p = [0, h, 2*h, 3*h]
x_c = [-2*h, -h, 0, h, 2*h]
x_r = [-3*h, -2*h, -h, 0]

H_p = calc_X(x_p)
H_c = calc_X(x_c)
H_r = calc_X(x_r)

H1_p = np.linalg.inv(H_p)
H1_c = np.linalg.inv(H_c)
H1_r = np.linalg.inv(H_r)


xi = 0
xf = 10

func = "(x**2)/2"
x = np.arange(xi, xf+h, h)
f = eval(func)

der = [0]*len(f)
# der2 = [0]*len(f)
# der3 = [0]*len(f)

for i in range (0, int(len(x_c)/2)): #Progressiva
    for j in range (0, len(x_p)):
        der[i] += H1_p[1][j]*f[i+j]
#         der2[i] += H1_p[2][j]*f[i+j]
#         der3[i] += H1_p[3][j]*f[i+j]
k = i+1

for i in range(k,len(f)-int(len(x_c)/2)):
    for j in range(0,len(x_c)):
        der[i] += H1_c[1][j]*f[i-int(len(x_c)/2)+j ] #fd[i] += coefc[1][j]*f[i-int(len(x_c)/2)+j ]
#         der2[i] += H1_c[2][j]*f[i-int(len(x_c)/2)+j]
#         der3[i] += H1_c[3][j]*f[i-int(len(x_c)/2)+j]
k = i+1

for i in range(k,len(f)):
    for j in range(0,len(x_r)):
        der[i] += H1_r[1][j]*f[i-len(x_r)+1+j]
#         der2[i] += H1_r[2][j]*f[i-len(x_r)+1+j]
#         der3[i] += H1_r[3][j]*f[i-len(x_r)+1+j]

print(der)
# print("\n", der2)
# print("\n", der3)
    
print_graf12(1,x,f,"x","f(x)","Função f(x)")
print_graf12(2,x,der,"x","f'(x)","Derivada 1ª Ordem de f(x)")

plt.show()
