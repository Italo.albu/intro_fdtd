import matplotlib.pyplot as plt
import numpy as np
import sympy as sp

## Código Análitico
xi = -np.pi
xf = np.pi

h = sp.Symbol('h')
H = sp.Matrix([[1,0,0,0],[1,h,h**2,h**3],[1,2*h,4*h**2,8*h**3],[1,3*h,9*h**2,27*h**3]])

f1 = sp.Symbol('f1')
f2 = sp.Symbol('f2')
f3 = sp.Symbol('f3')
f4 = sp.Symbol('f4')
funcao = sp.Matrix([[f1], [f2], [f3], [f4]])

print (H)
H1 = H.inv() ## Ou H1 = H**-1
print (H1)

A = H1*funcao
print (A)