import matplotlib.pyplot as plt
import numpy as np

###inicializando variaveis
list_dx = [1e-1, 5e-2, 1e-2, 5e-3, 1e-3]

xi = -np.pi
xf = np.pi

list_N = []        #número de elementos de cada lista x a depender do delta x
list_x = []        #eixos x para cada delta x
list_fx = []       #função de x para cada delta x
list_deriv2_fx = [] #derivada exata de x para cada delta x
N_dx = len(list_dx)

funcx = "np.sin(x)"
der2_funcx = "-np.sin(x)"

for i in range(0, N_dx):
    list_N.append(int((xf - xi)/list_dx[i]))
    list_x.append(np.linspace(xi,xf,list_N[i]))
    x = list_x[i]
    list_fx.append(eval(funcx))
    list_deriv2_fx.append(eval(der2_funcx))
    
#variáveis referentes a aproximação e erro
#aproximações de derivada de fx
fl2_p = []
fl2_r = []
fl2_c = []
#erro em cada tipo de aproximação
erro_p = []
erro_r = []
erro_c = []

media_erro_p = [0]*N_dx
media_erro_r = [0]*N_dx
media_erro_c = [0]*N_dx
##inicializando variáveis
for i in range(0, N_dx):
    fl2_p.append([0]*list_N[i])
    fl2_r.append([0]*list_N[i])
    fl2_c.append([0]*list_N[i])
    
    erro_p.append([0]*list_N[i])
    erro_r.append([0]*list_N[i])
    erro_c.append([0]*list_N[i])
    
### calculando derivadas e erro
for j in range(0, N_dx):
    for i in range(0, list_N[j]-2):
        fl2_p[j][i] = (list_fx[j][i+2] - 2*list_fx[j][i+1] + list_fx[j][i])/(list_dx[j]**2) #cálculo da derivada
        erro_p[j][i] = np.absolute((fl2_p[j][i] - list_deriv2_fx[j][i]))#cálculo do erro
        media_erro_p[j] += erro_p[j][i];#somatório para posterior cálculo da média
    for i in range(2,list_N[j]):
        fl2_r[j][i] = (list_fx[j][i] - 2*list_fx[j][i-1] + list_fx[j][i-2])/(list_dx[j]**2)
        erro_r[j][i] = np.absolute((fl2_r[j][i] - list_deriv2_fx[j][i]))
        media_erro_r[j] += erro_r[j][i];
    for i in range(1,list_N[j]-1):
        fl2_c[j][i] = (list_fx[j][i+1] - 2*list_fx[j][i] + list_fx[j][i-1])/(list_dx[j]**2)
        erro_c[j][i] = np.absolute((fl2_c[j][i] - list_deriv2_fx[j][i]))
        media_erro_c[j] += erro_c[j][i];

for i in range(0,N_dx):
    media_erro_p[i] /= list_N[i]-2
    media_erro_r[i] /= list_N[i]-2 #dois elementos indeterminados 
    media_erro_c[i] /= list_N[i]-2 
            
### plotando resultados
def print_graf(x, y, label_x, label_y, legend):
    plt.plot(x,y,label = legend)
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.legend()
    
#gráficos das aproximações
for i in range(0,N_dx):
    #gráficos das aproximaçoes
    plt.figure(i);
    plt.title("Comparação das derivadas para dx = %f" % list_dx[i])
    print_graf(list_x[i][:list_N[i]-3],fl2_p[i][:list_N[i]-3],"x","f'(x)", "Progressiva")
    print_graf(list_x[i][2:],fl2_r[i][2:],"x","f'(x)", "Regressiva")
    print_graf(list_x[i][1:list_N[i]-2],fl2_c[i][1:list_N[i]-2],"x","f'(x)", "Central")
    print_graf(list_x[i], list_deriv2_fx[i],"x","f'(x)", "Derivada exata de f(x)")

    #gráficos de erro
    plt.figure(i+N_dx);
    plt.title("Comparação dos erros para dx = %f" % list_dx[i])
    print_graf(list_x[i][:list_N[i]-3],erro_p[i][:list_N[i]-3],"x","Erro(%)", "Erro: Progressiva")
    print_graf(list_x[i][2:],erro_r[i][2:],"x","Erro(%)", "Erro: Regressiva")
    print_graf(list_x[i][1:list_N[i]-2],erro_c[i][1:list_N[i]-2],"x","Erro(%)", "Erro: Central")


#erro médio em cada derivada
print("dx\tEp(%)\tEr(%)\tEc(%)")
for i in range(0,N_dx):
    print("%.3f\t%.3f\t%.3f\t%.3f\t"%(list_dx[i],media_erro_p[i],media_erro_r[i],media_erro_c[i]))


plt.show()