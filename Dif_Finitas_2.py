import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse as sp
from scipy.sparse.linalg import spsolve

xi = 0
yi = 2
#inicial
xf = 2
yf = 0.2
#final

def Dx_Dxx (N, boundary = 'd'):
    Dxx = sp.diags([1, -2, 1], [-1, 0, 1], shape=(N, N)).toarray()
    Dx = sp.diags([-1, 0, 1], [-1, 0, 1], shape=(N, N)).toarray()
    
    if(boundary.lower() == 'p'):
        Dxx[0][-1] = 1
        Dxx[-1][0] = 1
        Dx[0][-1] = 1
        Dx[-1][0] = 1
    elif (boundary.lower() == 'n'):
        Dxx[0][0:N] = 0
        Dxx[-1][0:N] = 0
    else:
        print("Condição de Fronteira não definida.")
        
    return [Dx, Dxx]

boundary = input("Qual a condição de fronteira desejada? Dirichlet - d / Periódico - p / Neuman - n\n")
<<<<<<< HEAD
N = 100

=======
N = 11
>>>>>>> 9852df79479eb85dbb39a227611aa6916ce73d7b
dx = (xf - xi)/(N-1)

[Dx, Dxx] = Dx_Dxx(N, boundary)

I = sp.diags([1], [0], shape=(N, N)).toarray()

print (Dx, "\n")
print (Dxx, "\n")
print (I, "\n")

A = Dxx/(dx**2) + (5*Dx)/(2*dx) + 6*I
A[0][0]=1
A[0][1]=0
A[-1][-1]=1
A[-1][-2]=0


print (A, "\n")

B = np.zeros(N)
B[0] = yi
B[-1] = yf

A = sp.csr_matrix(A)
F = spsolve(A,B)

x = np.linspace(xi, xf, N)

a2 = (0.2*(np.exp(6))-2)/(np.exp(2)-1)
a1 = 2 - a2

f = a1*np.exp(-3*x) + a2*np.exp(-2*x) 

plt.figure("Comparação das funções obtidas")
plt.subplot(1,2,1)
plt.title("Cálculo por Diferenças Finitas (dx=%f)"%dx)
plt.plot(x,F)
plt.xlabel('x')
plt.ylabel('y')
plt.grid()

plt.subplot(1,2,2)
plt.title("Cálculo Análitico")
plt.plot(x,f)
plt.xlabel('x')
plt.ylabel('y')
plt.grid()

plt.tight_layout()
plt.show()
