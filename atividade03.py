## Inclusão da biblioteca numpy, para uso das operações com vetores
import numpy as np

## Função para o cálculo de X
def calculoX(x):
    tam = len(x)
    X = [[0]*tam]*tam
    for i in range(0,tam):
        vet = [0]*tam
        vet[0] = 1
        for j in range(1, tam):
            vet[j] = x[i]**j
        X[i] = vet
    X = np.array(X)
    return X

## Função para o cálculo de F
def calculoF(xi, h, n, funcao):
    F = [0]*n
    for i in range(0,n):
        x = xi + i*h
        F[i] = [eval(funcao)]
    return F

## Função para o cálculo de A
def calculoA(x,h, xi, funcao):
    tam = len(x)
   
    X = calculoX(x)
    F = calculoF(xi,h,tam,funcao)
    inv_X = np.linalg.inv(X)
    A = np.dot(inv_X,F)
    return A

## Código de execução
list_dx = [1e-1, 5e-2, 5e-3, 1e-3] #vetor de dx
funcao = "np.sin(x)" #função F
xi = 0 # valor inicial de x
tam_dx = len(list_dx)

for j in range (0, tam_dx):
    
    ##Calculo da matriz A
    h = list_dx[j] #Passo h = dx
    print("Para h = %f\n Função: %s" % (h,funcao))

    list_x_ = [[0,h,2*h], [-h,0,h], [-2*h,-h,0], [-2*h,-h,h,2*h]]
    N_vets_x_ = len(list_x_)
    list_A = [0]*N_vets_x_

    for i in range(0, N_vets_x_):
        list_A[i] = calculoA(list_x_[i], list_dx[i], xi, funcao)
        print("\nA[%d] = \n" % i, list_A[i])

 
