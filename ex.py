import sympy as sp

C = sp.Matrix([[4,-1,-1,0],[-1,4,0,-1],[-1,0,4,-1],[0,-1,-1,4]])
C1 = C.inv()
v1 = sp.Symbol('v1')
v2 = sp.Symbol('v2')
v3 = sp.Symbol('v3')
v4 = sp.Symbol('v4')
V = sp.Matrix([[v1],[v2],[v3],[v4]])
R = ([[10],[0],[10],[0]])

R1 = C1*R

print(R1)